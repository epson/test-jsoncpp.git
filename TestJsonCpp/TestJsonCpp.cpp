﻿// TestJsonCpp.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include "include/json/json.h"

#ifndef _DEBUG
#else
#ifdef _WIN64
#pragma comment (lib,"lib/x64/Debug/jsoncpp_static.lib")
#else
#pragma comment (lib,"lib/x86/Debug/jsoncpp_static.lib")
#endif // _WIN64
#endif

int main()
{
	Json::Value root;
	Json::Value value;
	Json::StreamWriterBuilder wbuilder;
	const std::unique_ptr<Json::StreamWriter> writer(wbuilder.newStreamWriter());

	value["id"] = 1;
	value["name"] = "Tom";
	root.append(value);
		
	value["id"] = 2;
	value["name"] = "Jerry";
	root.append(value);

	std::ostringstream json_str;

	std::cout << "Encode Json ===>" << std::endl;

	writer->write(root, &json_str);
	std::cout << json_str.str() << std::endl << std::endl;

	std::cout << "Decode Json ===>" << std::endl;
	
	JSONCPP_STRING err;
	Json::CharReaderBuilder rbuilder;
	const std::unique_ptr<Json::CharReader> reader(rbuilder.newCharReader());
	if (reader->parse(json_str.str().c_str(), json_str.str().c_str() + json_str.str().length(), &value, &err))
	{
		for (int i = 0; i < value.size(); i++) {
			const Json::Value o = value[i];
			int id = o["id"].asInt();
			std::string name(o["name"].asCString());

			std::stringstream _id;
			_id << id;

			std::cout << "id = " + _id.str() + "; name = " + name << std::endl;
		}
		
	}
}